package ku;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.chrono.ThaiBuddhistDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Methods for creating one or more Student objects
 * using CSV data.
 * @author jim
 */
public class StudentFactory {
	private static final String STUDENTFILE = "resource/students.csv";
	private List<Student> students;
	private static StudentFactory instance = null;
	
	private StudentFactory() {
		students = makeStudents();
	}
	
	/** Create an instance of the StudentFactory.  This method is not thread-safe.
	 * @return single instance of the StudentFactory
	 */
	public static StudentFactory getInstance() {
		if (instance == null) instance = new StudentFactory();
		return instance;
	}
	
	/** Get the students. */
	public List<Student> getStudents() {
		return students;
	}
	
	/**
	 * Load student data from CSV file or URL.
	 * @return List of students
	 */
	private List<Student> makeStudents()  {
		ClassLoader loader = this.getClass().getClassLoader();
		InputStream in = loader.getResourceAsStream(STUDENTFILE);
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		List<Student> students = new ArrayList<>();
		String data = null;
		int linenum = 0;
		do {
			try {
				data = reader.readLine();
				if (data == null) break;
			} catch (IOException e) {
				System.err.println("Error reading student data: " + e.getMessage());
				return students;
			}
			linenum++;
			if (data.startsWith("#")) continue; // skip comment lines
			// fields are: id,firstname,lastname,sex,dd/mm/yyyy
			String [] fields = data.split("\\s*,\\s*");
			if (fields.length != 5) {
				System.err.println("Invalid student data on line "+linenum);
				continue;
			}
			String lastname = titleCase( fields[2] );
			String fullname = titleCase( fields[1] )+" "+lastname;
			char sex = ' ';
			if (fields[3].length() == 1) sex = Character.toUpperCase(fields[3].charAt(0));
			else System.err.println("invalid sex field on line "+linenum);
			LocalDate birthday = parseDate( fields[4] );

			Student s = new Student(fields[0], fullname, sex, birthday );
			students.add(s);
		} while( true );
		return students;
	}
	
	private static String titleCase(String name) {
		return Character.toUpperCase(name.charAt(0)) + name.substring(1).toLowerCase();
	}

	/** Parse date in the format "mm/dd/yyyy" */
	private static LocalDate parseDate(String datestr) {
		final DateTimeFormatter format = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate date = LocalDate.parse(datestr.trim(), format);
		// fix the year?
		return date;
	}

	/** Test the student factory. */
	public static void main(String[] args) {
		StudentFactory factory = StudentFactory.getInstance();
		List<Student> students = factory.getStudents();
		for(Student s: students) System.out.println( s );
	}
}
